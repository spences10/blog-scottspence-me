import styled from 'styled-components';

export const BlockQuote = styled.blockquote`
  border-left: 5px solid #ccc;
  padding-left: 5px;
  font-style: italic;
`;
